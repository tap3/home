+++
description = "Technical details"
slug = "tech"
title = "Technical details"
layout = "page"
+++
## Services :

#### TT-Rss: 
Tiny Tiny RSS is a free and open source web-based news feed (RSS/Atom) reader and aggregator.   
source-code: https://git.tt-rss.org/git/tt-rss

#### Rss-Bridge

The RSS feed for websites missing it.  
source-code: https://github.com/RSS-Bridge/rss-bridge

#### EchoIP
IP address lookup service.  
source-code: https://github.com/mpolden/echoip

#### IPMagnet
ipMagnet allows you to see which IP address your BitTorrent Client is handing out to its peers and trackers!  
source-code: https://github.com/cbdevnet/ipmagnet 
  
#### AllTube
HTML GUI for youtube-dl.  
code-source: https://github.com/Rudloff/alltube

#### Keeweb
Online password manager compatible with KeePass/X/XC.  
source-code: https://github.com/keeweb/keeweb

## Server :

|       |       |
|:-------|-------:|
| CPU : |     1core |
| RAM : |     512Mb |
| Swap : |     None |
| Location : |     Sweden |
| Virtualization : |     OpenVZ |
