+++
description = "About page"
slug = "about"
title = "About Tap3"
layout = "page"
+++

## Contact :
<i class="fa fa-envelope" title="e-mail"></i> <a href="mailto:contact@tap3.eu"><i style="unicode-bidi: bidi-override;direction: rtl;">ue.3pat@tcatnoc</i></a><br />
<i class="fa fa-key" title="pgp key"></i> <a class="icon" target="_blank" href="/ed25519.txt" title="pub_key">4a15c3e7936522e42ca6f315ecd373a9379af917</a> [ECC (ed25519)]  
<i class="fa fa-key" title="pgp key"></i> <a class="icon" target="_blank" href="/rsa_4096.txt" title="pub_key">370dfe4c797b4670bcf82e54ef70b27eab29c69e</a> [RSA (4096)]  

## Donate :

<i class="fa fa-btc" title="bitcoin"></i> 14TWqLbXPLoFF1k47WH3HJ7KLQufAiYpE8<br />
<i class="fa fa-liberapay" title="liberapay"></i> <a href="https://liberapay.com/tap3" title="Tap3's liberapay account">liberapay.com/tap3</a>

Why give? To pay the server first and then if there is enough money left I will take a domain name.

## Credits :

<span>Background Pix by </span><a style="" href="https://unsplash.com/@wenniel" target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from Wenniel Lun"><span>Wenniel Lun</span></a>
